---
id: sprints-q2-2-2019
title: Sprint Q2-2-2019
---

From 08.05.2019 to 21.05.2019

## Goals

1. Code as doc solution is implemented
2. Test coverage > 80%
3. Theming setup is functional
4. i18n solution is documented and implemented

## Stories

### Increase unit test coverage

![loading-spinner.service.ts](/puml/test.puml)

### Implement doc-as-code solution

TBA

### Setup theming

TBA

### Implement i18n solution in every created component

TBA

### Find reusable services in legacy projects

TBA

#### Unit testing

TBA

### Implement sdk

TBA

### Create navigation service

#### Description

Every Module, which need an entry point for navigation needs to register its entry point against a service. The side navigation menu should react on a feature set provided by the navigation service.

#### Concept

![loading-spinner.service.ts](/puml/navigation.service.puml)

### Create loading spinner service

#### Description

As the a user i should not interact with the application while it recieves asynchronous data.

#### Concept

![loading-spinner.service.ts](/puml/loading-spinner.service.puml)