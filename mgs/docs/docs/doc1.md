---
id: functional-specification-theming
title: Theming
---

## The Problem

The meine-gesundheit-eportal supports multiple tenants with their own look and feel. In the past it costs several man days to add a new tenants.

## The Idea

We want to have a clear structured theming setup. We want to be able to switch a theme by just adjusting colors and typography.

## The Solution

Angular Material provides a theming solution.

### Application Theming Structure

In the application package is a styles.scss. This file aggregates information color.scss and typography.scss, which are in the same package. It also injects the theming.scss from angular 
material. From the shared package the main-theme.scss is included. The main-theme.scss aggregates every custom component theme.

![Theming](/puml/theming.puml)

### Theming an Angular Material app

First of all please read:
https://material.angular.io/guide/theming

To create a new theme in a new application you have to do the following steps.

* import angular material theming.scss in the main.scss file
  * @import '~@angular/material/theming';
* include mat-core();
* create a colors-paletts.scss and a typography.scss
* import color-paletts.scss and typography.scss into the main file
  * To create an angular material theme you need at least two color palettes. One for primary and one for accent colors.
  * A warn color palette is optional
  * typography is optional
* assign both color palletes (primary and accent) with the method mat-palette() to a local varibale
  * e.g. $tenant-app-primary: mat-palette($mat-indigo);
* create a theme with a material theming function
  * $tenant-app-theme: mat-light-theme($tenant-app-primary, $tenant-app-accent);
* include angular-material-theme to assign your theme
  * @include angular-material-theme($tenant-app-theme);

Example for an angular material color palette

        $mat-indigo: (
            50: #e8eaf6,
            100: #c5cae9,
            200: #9fa8da,
            300: #7986cb,
            400: #5c6bc0,
            500: #3f51b5,
            600: #3949ab,
            700: #303f9f,
            800: #283593,
            900: #1a237e,
            A100: #8c9eff,
            A200: #536dfe,
            A400: #3d5afe,
            A700: #304ffe,
            contrast: (
                50: $dark-primary-text,
                100: $dark-primary-text,
                200: $dark-primary-text,
                300: $light-primary-text,
                400: $light-primary-text,
                500: $light-primary-text,
                600: $light-primary-text,
                700: $light-primary-text,
                800: $light-primary-text,
                900: $light-primary-text,
                A100: $dark-primary-text,
                A200: $light-primary-text,
                A400: $light-primary-text,
                A700: $light-primary-text,
            )
        );

### Create a custom component theme

First of all please read:
https://material.angular.io/guide/theming-your-components

* create a new theme file in libs/shared/scss/components/
* add a mixing that expects a theme as input
* define everthing that is needed for the component inside the mixin

Example:

        @mixing button-theme($theme) {
            $accent: map-get($theme, accent);
            .mat-stroked-button {
                border-color: mat-color($accent);
            }
        }

### Create a tenant specific component theme

Follow the steps from the previous section. The theme file will be placed inside the application package. Ensure, that this file is included after the main-theme.scss.

### Create a custom typography

TBA
