module.exports = {
  name: 'features-main-navigation',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/features/main-navigation',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
