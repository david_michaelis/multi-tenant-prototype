import { async, TestBed } from '@angular/core/testing';
import { FeaturesMainNavigationModule } from './features-main-navigation.module';

describe('FeaturesMainNavigationModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FeaturesMainNavigationModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(FeaturesMainNavigationModule).toBeDefined();
  });
});
