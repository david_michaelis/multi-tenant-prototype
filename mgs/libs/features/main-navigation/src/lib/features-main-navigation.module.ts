import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route, Router } from '@angular/router';
import { MainNavigationComponent } from './main-navigation/main-navigation.component';
import { SharedModule } from '@mgs/shared';
import { FeaturesDashboardModule, featuresDashboardRoutes } from '@mgs/features/dashboard';
import { InvoiceManagementModule, invoiceManagementRoutes } from '@mgs/invoice-management';
import { MatSidenavModule } from '@angular/material';
import { featuresElectronicHealthRecordRoutes, FeaturesElectronicHealthRecordModule } from '@mgs/features/electronic-health-record';
import { FlexLayoutModule } from '@angular/flex-layout';

const featuresMainNavigationRoutes: Route[] = [
  {
    path: 'main',
    component: MainNavigationComponent,
    children: [].concat(
      featuresDashboardRoutes, 
      invoiceManagementRoutes,
      featuresElectronicHealthRecordRoutes
      )
  }
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    InvoiceManagementModule,
    FeaturesDashboardModule,
    FeaturesElectronicHealthRecordModule,
    MatSidenavModule,
    RouterModule.forChild(featuresMainNavigationRoutes),
    SharedModule,
  ],
  declarations: [MainNavigationComponent],
  exports: [MainNavigationComponent]
})
export class FeaturesMainNavigationModule {
  constructor(router: Router) {
    console.log(router.config)
  }
}
