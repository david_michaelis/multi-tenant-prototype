module.exports = {
  name: 'features-login',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/features/login',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
