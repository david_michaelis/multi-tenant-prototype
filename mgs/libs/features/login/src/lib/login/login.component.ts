import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup, FormBuilder} from '@angular/forms';
import { MyErrorStateMatcher } from './login-error-state-matcher';
import { Router } from '@angular/router';

@Component({
  selector: 'mgs-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private router: Router, private formBuilder: FormBuilder) {}

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
        email:  new FormControl('', [Validators.required, Validators.email]),
        password:  new FormControl('', [Validators.required]),
      });
  }

  matcher=new MyErrorStateMatcher();

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  login() {
    this.router.navigateByUrl('/main/dashboard');
  }
}
