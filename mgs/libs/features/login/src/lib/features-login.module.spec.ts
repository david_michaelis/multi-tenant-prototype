import { async, TestBed } from '@angular/core/testing';
import { FeaturesLoginModule } from './features-login.module';

describe('FeaturesLoginModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FeaturesLoginModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(FeaturesLoginModule).toBeDefined();
  });
});
