import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCardModule, MatInputModule } from '@angular/material';
import {NavigationService} from '@mgs/navigation-serivce';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export const featuresLoginRoutes: Route[] = [
  {
    path: 'login', 
    component: LoginComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(featuresLoginRoutes)
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent]
})
export class FeaturesLoginModule {
  constructor(private navigationService: NavigationService) {
    this.navigationService.forState('Login', '/login');
  }
}
