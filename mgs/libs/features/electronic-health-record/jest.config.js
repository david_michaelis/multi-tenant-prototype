module.exports = {
  name: 'features-electronic-health-record',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/features/electronic-health-record',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
