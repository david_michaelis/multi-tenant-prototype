import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { DetailsComponent } from './details/details.component';

import { MatButtonModule, MatCardModule, MatListModule } from '@angular/material';
import {NavigationService} from '@mgs/navigation-serivce';


export const featuresElectronicHealthRecordRoutes: Route[] = [
  {
    path: 'ehr-overview',
    component: OverviewComponent
  },
  {
    path: 'ehr-details',
    component: DetailsComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule, MatCardModule, MatButtonModule, MatListModule],
  declarations: [OverviewComponent, DetailsComponent],
  exports: [DetailsComponent, OverviewComponent]
})
export class FeaturesElectronicHealthRecordModule {
  constructor(private navigationService: NavigationService) {
    this.navigationService.forState('Electronic Health Record', 'ehr-overview');
  }
}
