import { async, TestBed } from '@angular/core/testing';
import { FeaturesElectronicHealthRecordModule } from './features-electronic-health-record.module';

describe('FeaturesElectronicHealthRecordModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FeaturesElectronicHealthRecordModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(FeaturesElectronicHealthRecordModule).toBeDefined();
  });
});
