import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatButtonModule, MatToolbarModule, MatCardModule, MatGridListModule } from '@angular/material';

export const featuresDashboardRoutes: Route[] = [
  {
    path: 'dashboard',
    component: DashboardComponent,
  }
];

@NgModule({
  imports: [CommonModule, RouterModule, MatButtonModule, MatToolbarModule, MatCardModule, MatGridListModule],
  declarations: [DashboardComponent],
  exports: [DashboardComponent]
})
export class FeaturesDashboardModule {
  
}
