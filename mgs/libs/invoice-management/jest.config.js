module.exports = {
  name: 'invoice-management',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/invoice-management',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
