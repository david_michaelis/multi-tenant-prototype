import { async, TestBed } from '@angular/core/testing';
import { InvoiceManagementModule } from './invoice-management.module';

describe('InvoiceManagementModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [InvoiceManagementModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(InvoiceManagementModule).toBeDefined();
  });
});
