import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { InvoiceManagementComponent } from './invoice-management/invoice-management.component';
import { MatButtonModule, MatCardModule, MatListModule } from '@angular/material';

import {NavigationService} from '@mgs/navigation-serivce';

export const invoiceManagementRoutes: Route[] = [
  {
    path: 'invoice',
    component: InvoiceManagementComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule, MatButtonModule, MatCardModule, MatListModule],
  declarations: [InvoiceManagementComponent]
})
export class InvoiceManagementModule {
  constructor(private navigationService: NavigationService) {
    this.navigationService.forState('Invoice Management', 'invoice');
  }
}
