/// <reference path="../../../../tools/custom-typings.d.ts" />
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TitleComponent } from './title/title.component';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable, from } from 'rxjs';

export function AddLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/');
}

export class WebpackTranslateLoader implements TranslateLoader {
  constructor() {

    console.log('ui module');
  }
  getTranslation(lang: string): Observable<any> {
    console.log('asd', lang)
    return from(System.import(`../assets/i18n/de.json`));
  }
}

@NgModule({
  imports: [
    CommonModule, 
    MatButtonModule, 
    MatCheckboxModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: WebpackTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [TitleComponent],
  exports: [TitleComponent, MatButtonModule, MatCheckboxModule, TranslateModule]
})
export class UiModule {
  constructor(translateService: TranslateService) {
    console.log('translateService', translateService);
    translateService.use('de');
  }
}
