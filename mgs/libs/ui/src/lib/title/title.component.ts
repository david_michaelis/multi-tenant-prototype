import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mgs-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {
  @Input() title: string;
  constructor() { }
  param = {value: 'bar2bar'}
  
  ngOnInit() {
    console.log('init title console!', this);
  }

}
