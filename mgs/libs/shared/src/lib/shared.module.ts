import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SideNavigationComponent } from './components/side-navigation/side-navigation.component';
import { MatToolbarModule, MatListModule, MatIconModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [CommonModule, MatToolbarModule, MatListModule, BrowserAnimationsModule, MatIconModule, RouterModule],
  declarations: [HeaderComponent, FooterComponent, SideNavigationComponent],
  exports: [HeaderComponent, FooterComponent, SideNavigationComponent]
})
export class SharedModule {}
