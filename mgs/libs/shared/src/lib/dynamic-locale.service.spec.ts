import { TestBed } from '@angular/core/testing';

import { DynamicLocaleService } from './dynamic-locale.service';

describe('DynamicLocaleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DynamicLocaleService = TestBed.get(DynamicLocaleService);
    expect(service).toBeTruthy();
  });
});
