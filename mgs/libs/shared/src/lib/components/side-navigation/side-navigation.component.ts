import { Component, OnInit } from '@angular/core';
import { NavigationService } from '@mgs/navigation-serivce';
import orderBy from 'lodash/orderBy';

@Component({
  selector: 'mgs-side-navigation',
  templateUrl: './side-navigation.component.html',
  styleUrls: ['./side-navigation.component.scss']
})
export class SideNavigationComponent implements OnInit {
  entries = [];
  
  constructor(private navigationService: NavigationService) {
    
  }
  
  ngOnInit() {
    this.entries = orderBy(this.navigationService.entries, 'translation');
  }
}
