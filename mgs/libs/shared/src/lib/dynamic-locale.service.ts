import { Injectable } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeNorwegian from '@angular/common/locales/nb';
import localeSwedish from '@angular/common/locales/sv';

@Injectable({
  providedIn: 'root'
})
export class DynamicLocaleService {
  locale;

  constructor() { }
  
  registerCulture(culture: string) {
    if (!culture) {
        return;
    }
    this.locale = culture;

    // Register locale data since only the en-US locale data comes with Angular
    switch (culture) {
        case 'nb-NO': {
            registerLocaleData(localeNorwegian);
            break;
        }
        case 'sv-SE': {
            registerLocaleData(localeSwedish);
            break;
        }
    }
  }
}
