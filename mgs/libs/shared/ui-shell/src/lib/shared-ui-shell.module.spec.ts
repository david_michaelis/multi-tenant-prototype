import { async, TestBed } from '@angular/core/testing';
import { SharedUiShellModule } from './shared-ui-shell.module';

describe('SharedUiShellModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedUiShellModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SharedUiShellModule).toBeDefined();
  });
});
