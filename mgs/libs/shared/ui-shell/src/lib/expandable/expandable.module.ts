import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExpandableRoutingModule } from './expandable-routing.module';
import { ExpandableComponent } from './expandable.component';

@NgModule({
  declarations: [ExpandableComponent],
  imports: [
    CommonModule,
    ExpandableRoutingModule
  ]
})
export class ExpandableModule { }
