import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { ExpandableComponent } from './expandable/expandable.component';
import { ButtonModule } from './button/button.module';
import { ButtonComponent } from './button/button.component';

export const sharedUiShellRoutes: Route[] = [];

@NgModule({
  imports: [
    ButtonModule,
    CommonModule, 
    RouterModule
  ],
  declarations: [ExpandableComponent],
  exports: [ButtonModule]
})
export class SharedUiShellModule {}
