module.exports = {
  name: 'shared-ui-shell',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/shared/ui-shell',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
