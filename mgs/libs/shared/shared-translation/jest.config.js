module.exports = {
  name: 'shared-shared-translation',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/shared/shared-translation',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
