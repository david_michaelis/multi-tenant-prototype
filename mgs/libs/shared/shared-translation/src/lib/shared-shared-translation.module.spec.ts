import { async, TestBed } from '@angular/core/testing';
import { SharedSharedTranslationModule } from './shared-shared-translation.module';

describe('SharedSharedTranslationModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedSharedTranslationModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SharedSharedTranslationModule).toBeDefined();
  });
});
