import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationService } from './navigation.service';

@NgModule({
  imports: [CommonModule],
  exports: [NavigationService]
})
export class CoreModule {}
