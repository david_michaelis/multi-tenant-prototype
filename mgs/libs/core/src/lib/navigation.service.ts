import { Injectable } from '@angular/core';
import {  Route } from '@angular/router';

export let childRoutes:Route[] = [];

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  entries = [];

  forState(translation: string, path: string) {
    this.entries = [
      ...this.entries, 
      {
        translation, 
        path
      }
    ]
  }
}
