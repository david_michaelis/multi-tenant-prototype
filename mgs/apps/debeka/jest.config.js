module.exports = {
  name: 'debeka',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/debeka/',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
