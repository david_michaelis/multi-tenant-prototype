module.exports = {
  name: 'axa',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/axa/',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
