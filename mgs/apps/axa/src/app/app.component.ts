import { Component } from '@angular/core';
import {test} from 'test.js';

@Component({
  selector: 'mgs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'axa';
  translateValue = {value: 'AXA'};
  constructor() {

    console.log(test);
  }
}
