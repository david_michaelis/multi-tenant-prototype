import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';

import { FeaturesLoginModule } from '@mgs/features/login';

import { FeaturesMainNavigationModule } from '@mgs/features/main-navigation';
import { SharedModule } from '@mgs/shared';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    FeaturesLoginModule,
    FeaturesMainNavigationModule,
    BrowserModule,
    SharedModule,
    RouterModule.forRoot(routes, { initialNavigation: 'enabled' })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
